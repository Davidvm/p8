<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH .'/app/models/UsuarioModel.php';


class RegisterModel extends Database {

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function insertUser(Usuario $data){
        $connection = Database::instance();

        try {

            $stmt = $connection->prepare("SELECT * FROM Usuario WHERE email=:email");
            $stmt->execute(array(":email"=>$data->email));
            $count = $stmt->rowCount();

            if($count==0){

                $stmt = $connection->prepare("INSERT INTO usuario (username,nombre,apellidos,email, telefono,contrasena,fechaNacimiento,fechaRegistro) 
                    VALUES (:uname, :nombre, :apellidos, :email, :telefono, :pass, :fnan, :jdate)");
                $stmt->bindParam(":uname",$data->username);
                $stmt->bindParam(":nombre",$data->nombre);
                $stmt->bindParam(":apellidos",$data->apellidos);
                $stmt->bindParam(":email",$data->email);
                $stmt->bindParam(":telefono",$data->telefono);
                $stmt->bindParam(":pass",$data->contrasena);
                $stmt->bindParam(":fnan",$data->fechaNacimiento);
                $stmt->bindParam(":jdate",$data->joinDate);

                if($stmt->execute()) {

                    $id = $connection->lastInsertId();
                    return $id;

                } else {

                    echo "Query could not execute !";
                }
            } else { 

                echo "1"; //  not available
            }

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}