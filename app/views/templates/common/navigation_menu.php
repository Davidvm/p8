<!-- Start Header Area -->
<header class="header_area sticky-header">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="home"><img src="<?php echo FOLDER_PATH . 'app/assets/img/logo.png'?>" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="<?php echo FOLDER_PATH . 'home'?>">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo FOLDER_PATH . 'nosotros'?>">Nosotros</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo FOLDER_PATH . 'actividades'?>">Actividades</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo FOLDER_PATH . 'contacto'?>">Contacto</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item">
                            <a href="<?php echo FOLDER_PATH . 'login'?>"><span class="lnr lnr-user"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- End Header Area -->