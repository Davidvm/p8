<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH . '/app/models/CategoriasModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/ActividadesModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Login controller
*/
class CategoryProcessController {
    private $model;
    private $session;

    public function __construct() {
        $this->model = new Actividades();
        $this->session = new Session();
    }

    public function exec($request_params) {

		if(isset($_POST["action"])) {

	        if(isset($_REQUEST["category"])){
	           $category_filter = implode(",", $_REQUEST["category"]);
	        } else {
                $category_filter = '1,2,3';
            }

	        $result = $this->model->getActivitiesBySearch($category_filter);

	        echo $result;
	    }
    }
}