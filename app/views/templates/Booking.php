
<?php session_start(); ?>

<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/karma/app/assets/img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="CodePixar">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Karma Reserva Actividad</title>
  <!--
    CSS
    ============================================= -->
<?php include(ROOT . "/karma/app/views/templates/common/styles.php"); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>

<body>

<?php include(ROOT . "/karma/app/views/templates/common/navigation_menu.php"); ?>

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Reserva</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Reserva Actividad</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->
    <section class="checkout_area section_gap">
        <div class="container">

          <?php  if (!isset($_SESSION['username'])) { ?>
            <div class="returning_customer">
                <div class="check_title">
                    <h2>¿Todavia no eres cliente? <a href="<?php echo FOLDER_PATH . 'register'?>">Haz clic aqui para registrarte</a> o completa el formulario de Reserva</h2>
                </div>
                <p>Si ya has reservado alguna experiencia con nosotros, por favor introduce tu usuario. Si eres nuevo usuario, completa el siguiente formulario.</p>
                <form class="row contact_form" id="loginForm" method="post">
                    <div class="col-md-6 form-group p_star">
                     <input type="text" class="form-control" id="username" name="username" placeholder="Usuario" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Usuario'">
                    </div>
                    <div class="col-md-6 form-group p_star">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                    </div>
                    <div class="col-md-12 form-group">
                        <div id="msg_login" class="message"><?php !empty($error_message) ? print($error_message) : '' ?></div>
                        <button type="submit" value="submit" class="primary-btn">login</button>
                    </div>
                </form>
            </div>
            <?php } ?>
            <div class="billing_details">
                <div class="row">
                    <div class="col-lg-8">
                        <?php  if (isset($_SESSION['username'])) { ?>
                        <h2 class="welcome-login"> <?php echo 'Hola ' . $_SESSION['username']; ?></h2>
                        <?php } ?>
                        <h2 class="welcome-login-ajax hidden"></h2>
                        <h3>Detalles de Reserva</h3>
                        <form class="row booking_form" id="bookingForm" action="#" method="post">
                            <input type="hidden" name="pvp_total" value="<?php echo $actividad['pvp'] ?>">
                            <input type="hidden" name="actividad" value="<?php echo $actividad['id'] ?>">
                            <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo isset($_SESSION['id']) ? $_SESSION['id'] : '0';?>">

                            <div class="col-md-6 form-group p_star">
                                 <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre'">
                            </div>
                            <div class="col-md-6 form-group p_star">
                                 <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Apellidos'">
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Teléfono'">
                            </div>
                            <div class="col-md-6 form-group p_star">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'">
                            </div>
                            
                            <div class="col-md-4 form-group p_star">
                                <select class="country_select" id="dia" name="dia">
                                    <option value="">Seleccionar dia</option>
                                    <?php foreach ($dias as $dia) { ?>
                                        <option value="<?php echo $dia['id'] ?>"><?php echo date_format(new DateTime($dia['dia']), "d/m/Y") ?></option>
                                    <?php  } ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group p_star">
                                <select class="country_select" id="hora" name="hora">
                                    <option value="">Seleccionar hora</option>
                                    <?php foreach ($horas as $hora) { ?>
                                        <option value="<?php echo $hora['id'] ?>"><?php echo date('H:i',strtotime($hora['inicioActividad'])) ?>-<?php echo date('H:i',strtotime($hora['finActividad'])) ?></option>
                                    <?php  } ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group p_star">
                                <select class="country_select participantes" id="participantes" name="participantes">
                                    <option value="">Seleccionar participantes</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <?php  if (!isset($_SESSION['id'])) { ?>
                            <div class="creat_account">
                                <div class="col-md-12">
                                    <h3>Datos de Usuario</h3>
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Nombre de Usuario*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" id="datepicker" class="form-control" name="fechaNacimiento" placeholder="Fecha de Nacimiento*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Repite la Contraseña*">
                                </div>
                            </div>
                            <?php } ?>

                            <div class="col-md-12 form-group">
                                <div class="other_details">
                                    <h3>Otros detalles</h3>
                                </div>
                                <textarea class="form-control" name="nota" id="nota" rows="1" placeholder="Otras notas"></textarea>
                            </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="order_box">
                            <h2>Tu Reserva</h2>
                            <ul class="list">
                                <li><a href="#">Actividad <span>Total</span></a></li>
                                <li><a href="#"><?php echo $actividad['nombre'] ?> <span class="middle participantes_count">x 1</span> <span class="last pvp_item"><?php echo $actividad['pvp'] ?>€</span></a></li>
                            </ul>
                            <ul class="list list_2">
                                <li><a href="#">Subtotal <span class="subtotal"><?php echo $actividad['pvp'] ?>€</span></a></li>
                                <li><a href="#">Total <span class="total"><?php echo $actividad['pvp'] ?>€</span></a></li>
                            </ul>
                             <button type="submit" value="submit" class="primary-btn">Proceder a la reserva</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--================End Checkout Area =================-->

    <!--================Contact Success and Error message Area =================-->
    <div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <h2>Gracias</h2>
                    <p>Tu reserva se ha realizado correctamente, te hemos enviado un email con los datos.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <h2>Lo sentimos!</h2>
                    <p> Algo ha ido mal. Prueba de nuevo.</p>
                </div>
            </div>
        </div>
    </div>
    <!--================End Contact Success and Error message Area =================--> 

<?php include(ROOT . "/karma/app/views/templates/common/footer.php"); ?>
<?php include(ROOT . "/karma/app/views/templates/common/scripts.php"); ?>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

    $( "#datepicker" ).datepicker({
        dateFormat: "dd-mm-yy"
    });

        $('.participantes').on('change', function(){
            $participantes = this.value;
            $pvp = parseInt($('.pvp_item').text());
            $total = $participantes*$pvp
            $('.participantes_count').text('x ' + $participantes);
            $('.subtotal').text( $total + '€');
            $('.total').text($total + '€');
            $('input[name="pvp_total"]').val($total);
        })

        var form = $('#loginForm'); 

        form.on('submit', function(e) {
            e.preventDefault(); 

           
            $.ajax({
                url: '<?php echo FOLDER_PATH . 'loginprocess'?>',
                type: 'POST', 
                dataType: 'json',
                data: form.serialize(),
                success: function(data) {
                    if(data.id) {
                        $('.returning_customer').fadeOut('slow');
                        $('.creat_account').fadeOut('slow');
                        $('.user_dates').fadeOut('slow');
                        $('.welcome-login-ajax').text('Hola ' + data.username);
                        $('.welcome-login-ajax').fadeIn('slow');
                        $('#id_usuario').val(data.id);
                        $('#nombre').val(data.nombre);
                        $('#apellidos').val(data.apellidos);
                        $('#telefono').val(data.telefono);
                        $('#email').val(data.email);
                    } else {
                         $("#msg_login").html(data);
                    }
                },
                error: function(e) {
                    console.log(e);
                }
            });
        });

/* validation */
    $("#bookingForm").validate({
        rules:
        {
            nombre: {
                required: true,
            },
            apellidos: {
                required: true,
            },
            telefono: {
                required: true,
                number: true,
                maxlength: 15,
            },
            email: {
                required: true,
                email: true
            },
            dia: {
                required: true,
            },
        },
        messages:
        {
            email: "Introduce un email valido",
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm() {
      var form = $('#bookingForm'); 

          $.ajax({
              url: '<?php echo FOLDER_PATH . 'bookingprocess'?>', 
              type: 'POST', 
              dataType: 'json', 
              data: form.serialize(), 
              success: function(data) {
                console.log(data);
                if (data == 'success'){
                    $('#success').modal('show');
                    form.trigger('reset'); 
                    window.location.href = "<?php echo FOLDER_PATH . 'login'?>";
                } else {
                    $('#error').modal('show');
                }
                },
                error: function(e) {
                    console.log(e);
                }
        });
        return false;
    }

    });
</script>

</body>

</html>