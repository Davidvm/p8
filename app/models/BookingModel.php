<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Home Model
 */
class Booking extends Database {

  public function __construct()
  {
    parent::__construct();
  }


  public function getAlldaysOfActivity($id_actividad){
    $connection = Database::instance();
    try {
        $stm = $connection->prepare("SELECT * FROM dia_actividad JOIN actividad_detalles ON (actividad_detalles.diaId = dia_actividad.id ) WHERE actividad_detalles.idActividad = ?");
        $stm->execute(array($id_actividad));
        return $stm->fetchAll();
    } catch (Exception $e) {
        die($e->getMessage());
    }
  }

  public function getAllHoursOfActivity($id_actividad){
    $connection = Database::instance();
    try {
        $stm = $connection->prepare("SELECT * FROM horas_actividad JOIN actividad_detalles ON (actividad_detalles.diaId = horas_actividad.id ) WHERE actividad_detalles.idActividad = ?");
        $stm->execute(array($id_actividad));
        return $stm->fetchAll();
    } catch (Exception $e) {
        die($e->getMessage());
    }
  }


  public function getAllBookingsOfUser($id_usuario){
    $connection = Database::instance();
    try {
        $stm = $connection->prepare("SELECT a.id,a.nombre,a.Imagen_principal,r.idDia,d.dia FROM actividad a INNER JOIN reservas r ON r.idActividad = a.id INNER JOIN dia_actividad d ON d.id = r.idDia WHERE r.idUser = ?");
        $stm->execute(array($id_usuario));
        return $stm->fetchAll();
    } catch (Exception $e) {
        die($e->getMessage());
    }
  }


  public function insertBooking(Booking $data){
      $connection = Database::instance();

      try {

        $stmt = $connection->prepare("INSERT INTO reservas (idUser, idActividad, idDia, idHora, pax, pvp, estado, nota) VALUES (:user, :actividad, :idDia, :idHora, :pax, :pvp, :estado, :nota)");
        $stmt->bindParam(":user",$data->id);
        $stmt->bindParam(":actividad",$data->idActividad);
        $stmt->bindParam(":idDia",$data->idDia);
        $stmt->bindParam(":idHora",$data->idHora);
        $stmt->bindParam(":pax",$data->pax);
        $stmt->bindParam(":pvp",$data->pvp);
        $stmt->bindParam(":estado",$data->estado);
        $stmt->bindParam(":nota",$data->nota);
        $stmt->execute();

        if($stmt->execute()) {

            return 1;
            
        } else {

            return 0;
        }

      } catch (Exception $e) {
          die($e->getMessage());
      }
  }

}