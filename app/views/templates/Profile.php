<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/karma/app/assets/img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="CodePixar">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Perfíl</title>
  <!--
    CSS
    ============================================= -->
<?php include(ROOT . "/karma/app/views/templates/common/styles.php"); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<?php include(ROOT . "/karma/app/views/templates/common/navigation_menu.php"); ?>

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Perfíl</h1>
                    <nav class="d-flex align-items-center">
                        <a href="home">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Perfíl</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

   <!--================Profile Area =================-->
    <section class="blog_area single-post-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                        <aside class="single_sidebar_widget author_widget">
                            <img class="author_img rounded-circle" style="max-width: 150px" src="app/assets/img/blog/author.png" alt="">
                            <h4><?php echo $user['username']; ?></h4>
                            <p>Senior Experience</p>
                            <div class="social_icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </div>
                            <p>
                                <b>Nombre: </b> <?php echo $user['nombre']; ?><br>
                                <b>Apellidos: </b> <?php echo $user['apellidos']; ?><br>
                                <b>Email: </b> <?php echo $user['email']; ?><br>
                                <b>Fecha de nacimiento: </b> <?php echo date_format(new DateTime($user['fechaNacimiento']), "d/m/Y") ?> <br>
                                <a class="link_modify" href="#">Modificar tus datos</a>
                            </p>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Tus Actividades</h3>
                            <?php foreach ($activities as $activity) { ?>
                                <div class="media post_item">
                                    <img src="app/assets/img/activities/<?php echo $activity['Imagen_principal']; ?>" style="max-width: 100px;" alt="post">
                                    <div class="media-body">
                                        <a href="blog-details.html">
                                            <h3><?php echo $activity['nombre']; ?></h3>
                                        </a>
                                        <p><?php echo date_format(new DateTime($activity['dia']), "d-m-Y") ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget ads_widget">
                            <a href="#"><img class="img-fluid" src="app/assets/img/blog/add.jpg" alt=""></a>
                            <div class="br"></div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12">
                            <div class="feature-img">
                                <img class="img-fluid" src="app/assets/img/blog/feature-img1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 blog_details">
                            <h2>Te puede interesar...</h2>
                            <p class="excert">
                                MCSE boot camps have its supporters and its detractors. Some people do not understand
                                why you should have to spend money on boot camp when you can get the MCSE study
                                materials yourself at a fraction.
                            </p>
                            <p>
                                Boot camps have its supporters and its detractors. Some people do not understand why
                                you should have to spend money on boot camp when you can get the MCSE study materials
                                yourself at a fraction of the camp price. However, who has the willpower to actually
                                sit through a self-imposed MCSE training. who has the willpower to actually sit through
                                a self-imposed
                            </p>
                            <p>
                                Boot camps have its supporters and its detractors. Some people do not understand why
                                you should have to spend money on boot camp when you can get the MCSE study materials
                                yourself at a fraction of the camp price. However, who has the willpower to actually
                                sit through a self-imposed MCSE training. who has the willpower to actually sit through
                                a self-imposed
                            </p>
                        </div>
                        <div class="col-lg-12 col-md-12 container-update-form hidden m-t-30">
                             <h2>Modifica tus datos:</h2>
                            <form class="row contact_form m-t-30" id="updateForm" method="post">
                              <div id="msg" class="message"></div>
                                <input type="hidden" name="id_user" value="<?php echo $user['id']; ?>">
                                <input type="hidden" name="username" value="<?php echo $user['username']; ?>">
                                <div class="col-md-6 form-group p_star">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Repite la Contraseña*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" id="datepicker" class="form-control" name="fechaNacimiento" placeholder="Fecha de Nacimiento*">
                                </div>
                                <div class="col-md-6 form-group p_star">
                                    <input type="text" id="telefno" class="form-control" name="telefono" placeholder="Teléfono*">
                                </div>

                                <div class="col-md-12 form-group">
                                    <button type="submit" class="primary-btn" id="btn-submit">Modificar datos</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

<?php include(ROOT . "/karma/app/views/templates/common/footer.php"); ?>
<?php include(ROOT . "/karma/app/views/templates/common/scripts.php"); ?>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.link_modify').on('click', function(e){
            e.preventDefault(); 
            $('.blog_details').fadeOut('slow');
            $('.container-update-form').fadeIn('slow');
        })

        $( "#datepicker" ).datepicker({
            dateFormat: "dd-mm-yy"
        });

    /* validation */
    $("#updateForm").validate({
        rules:
        {
            nombre: {
                required: true,
            },
            apellidos: {
                required: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            telefono: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            user_name: "Introduce un Usuario valido",
            password:{
                required: "Introduce una contraseña",
                minlength: "La contraseña debe tener al menos 8 caracteres"
            },
            email: "Introduce un email valido",
            telefono: "Introduce un teléfono",
            cpassword:{
                required: "Repite la contraseña",
                equalTo: "Las contraseñas no son iguales"
            }
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm() {
      var form = $('#updateForm'); // contact form

          $.ajax({
              url: 'profileprocess', 
              type: 'POST', 
              dataType: 'json', 
              data: form.serialize(), 
              success: function(data) {
                    if(data.username) {
                        window.location.href = "profile";
                    } else {
                         $("#msg").html(data);
                    }
                },
                error: function(e) {
                    console.log(e);
                }
            });
        return false;
    }
    /* form submit */
    });
</script>
</body>

</html>