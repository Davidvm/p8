<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
* Login controller
*/
class RegisterController extends Controller {
  public $nombre;
  public $model;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->nombre = 'Registro';
  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();
  }

  /**
  * Método de ejemplo
  */
  public function show()
  {
    $params = array('nombre' => $this->nombre);
    $this->render(__CLASS__, $params); 
  }

}