<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH .'app/models/RegisterModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Register controller
*/
class RegisterProcessController {

    private $model;
    private $session;

    public function __construct() {
        $this->model = new RegisterModel();
        $this->session = new Session();
    }

    public function exec($request_params) {

        $user = new Usuario();

        $user->username = $_REQUEST['user_name'];
        $user->nombre = $_REQUEST['nombre'];
        $user->apellidos = $_REQUEST['apellidos'];
        $user->email = $_REQUEST['email'];
        $user->telefono = $_REQUEST['telefono'];
        $user->contrasena = password_hash( $_REQUEST['password'], PASSWORD_BCRYPT, array('cost' => 11));
        $user->fechaNacimiento = date("Y-m-d", strtotime($_REQUEST['fechaNacimiento']));
        $user->joinDate = date('Y-m-d');

        $result = $this->model->insertUser($user);

        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }
}