<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
 * Actividades Model
 */
class Actividades extends Database
{

    public $idActividad;
    public $nombre;
    public $descripcion;
    public $pax;
    public $idSubcategoria;
    public $pvp;
    public $horario;
    public $Imagen_principal;
    public $Imagen_secundaria_1;
    public $Imagen_secundaria_2;
    public $Imagen_secundaria_3;


    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * @param mixed $idActividad
     *
     * @return self
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPax()
    {
        return $this->pax;
    }

    /**
     * @param mixed $pax
     *
     * @return self
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdSubcategoria()
    {
        return $this->idSubcategoria;
    }

    /**
     * @param mixed $idSubcategoria
     *
     * @return self
     */
    public function setIdSubcategoria($idSubcategoria)
    {
        $this->idSubcategoria = $idSubcategoria;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPvp()
    {
        return $this->pvp;
    }

    /**
     * @param mixed $pvp
     *
     * @return self
     */
    public function setPvp($pvp)
    {
        $this->pvp = $pvp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * @param mixed $horario
     *
     * @return self
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagenPrincipal()
    {
        return $this->Imagen_principal;
    }

    /**
     * @param mixed $Imagen_principal
     *
     * @return self
     */
    public function setImangePrincipal($Imagen_principal)
    {
        $this->Imagen_principal = $Imagen_principal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagenSecundaria1()
    {
        return $this->Imagen_secundaria_1;
    }

    /**
     * @param mixed $Imagen_secundaria_1
     *
     * @return self
     */
    public function setImagenSecundaria1($Imagen_secundaria_1)
    {
        $this->Imagen_secundaria_1 = $Imagen_secundaria_1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagenSecundaria2()
    {
        return $this->Imagen_secundaria_2;
    }

    /**
     * @param mixed $Imagen_secundaria_2
     *
     * @return self
     */
    public function setImagenSecundaria2($Imagen_secundaria_2)
    {
        $this->Imagen_secundaria_2 = $Imagen_secundaria_2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagenSecundaria3()
    {
        return $this->Imagen_secundaria_3;
    }

    /**
     * @param mixed $Imagen_secundaria_3
     *
     * @return self
     */
    public function setImagenSecundaria3($Imagen_secundaria_3)
    {
        $this->Imagen_secundaria_3 = $Imagen_secundaria_3;

        return $this;
    }


    public function getAllActividades() {
        $connection = Database::instance();
        try {
            $stmt = $connection->prepare("SELECT * FROM actividad");
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getActividad($id) {
        $connection = Database::instance();
        try {
            $stmt = $connection->prepare("SELECT actividad.*, actividad_detalles.* FROM actividad INNER JOIN actividad_detalles ON (actividad.id = actividad_detalles.idActividad) WHERE actividad.id = ?");
            $stmt->execute(array($id));
            return $stmt->fetch();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function getActividadesByCategory($id_category){
        $connection = Database::instance();
        try {
            $stmt = $connection->prepare("SELECT * actividad WHERE idCategoria = ?");
            $stmt->execute(array($id_category));
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function getRamdonActividades($max_number) {
        $connection = Database::instance();
        try {
            $stmt = $connection->prepare("SELECT * FROM actividad ORDER BY RAND() LIMIT :number");
            $stmt->bindParam(":number",$max_number, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function getActivitiesBySearch($category_filter){
        $connection = Database::instance();
        try {
            $query = "
                SELECT * FROM actividad WHERE estado = '1'
            ";

            $query .= "
             AND idCategoria IN(".$category_filter.")
            ";

            $stmt = $connection->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $total_row = $stmt->rowCount();
            $output = '';
            if($total_row > 0)
            {
                foreach($result as $row)
                {
                    $output .= '
                    <!-- single product -->
                        <div class="col-lg-4 col-md-6">
                            <div class="single-product">
                                <img class="img-fluid" src="app/assets/img/activities/'. $row['Imagen_principal'] .'" alt="">
                                <div class="product-details">
                                    <h6>'. $row['nombre'] .'</h6>
                                    <div class="price">
                                    </div>
                                    <div class="prd-bottom">

                                        <a href="'. FOLDER_PATH .'booking/?id='. $row['id'] .'" class="social-info">
                                            <span class="ti-bag"></span>
                                            <p class="hover-text">Reservar</p>
                                        </a>
                                        <a href="'. FOLDER_PATH .'actividad/?id='. $row['id'] .'" class="social-info">
                                            <span class="lnr lnr-move"></span>
                                            <p class="hover-text">Ver más</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ';
                }
            }
            else
            {
                $output = '<div class="col-xs-12 col-sm-12 col-md-12"><h3>No se han encontrado actividades</h3></div>';
            }
            echo $output;



        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}