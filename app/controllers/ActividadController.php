<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/CategoriasModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/ActividadesModel.php';

/**
 * ActividadController
 */
class ActividadController extends Controller {

  public $categorias;
  public $actividad;
  public $id_actividad;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->categorias = new Categorias();
    $this->actividad = new Actividades();

  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();

  }


  public function show() {
      $id_actividad = $_GET['id'];
      $actividad = $this->actividad->getActividad($id_actividad);
      $categoria = $this->categorias->getCategoriaById($actividad['idCategoria']);
      $params = array('actividad' => $actividad, 'categoria' => $categoria);
      $this->render(__CLASS__, $params);
  }

}