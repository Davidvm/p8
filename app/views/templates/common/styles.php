<link rel="stylesheet" href="/karma/app/assets/css/linearicons.css">
<link rel="stylesheet" href="/karma/app/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="/karma/app/assets/css/themify-icons.css">
<link rel="stylesheet" href="/karma/app/assets/css/bootstrap.css">
<link rel="stylesheet" href="/karma/app/assets/css/owl.carousel.css">
<link rel="stylesheet" href="/karma/app/assets/css/nice-select.css">
<link rel="stylesheet" href="/karma/app/assets/css/nouislider.min.css">
<link rel="stylesheet" href="/karma/app/assets/css/ion.rangeSlider.css" />
<link rel="stylesheet" href="/karma/app/assets/css/ion.rangeSlider.skinFlat.css" />
<link rel="stylesheet" href="/karma/app/assets/css/magnific-popup.css">
<link rel="stylesheet" href="/karma/app/assets/css/main.css">
