<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/UsuarioModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/BookingModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
 * Cine controller
 */
class ProfileController extends Controller {
    private $user;
    private $booking;
    private $session;

    /**
     * Inicializa valores
     */
    function __construct() {
        $this->user = new Usuario();
        $this->booking = new Booking();
        $this->session = new Session();

        $this->session->init();
        if ($this->session->getStatus() === 1 || empty($this->session->get('username')))
            exit('Acceso denegado');
    }


    private function verify($request_params) {
        return empty($request_params['nombre']) OR empty($request_params['apellidos']) OR empty($request_params['email'])
            OR empty($request_params['password']) OR empty($request_params['repassword']) OR empty($request_params['fechaNacimiento']);
    }

    private function renderErrorMessage($message) {
        $params = array('error_message' => $message);
        $this->render(__CLASS__, $params);
    }


    /**
     * Método estándar
     */
    public function exec() {
        $this->show();
    }

    /**
     * Método de ejemplo
     */
    public function show() {
        $params = array('user' => $this->user->getProfileUser($this->session->get('id')), 'activities' => $this->booking->getAllBookingsOfUser($this->session->get('id')));
        $this->render(__CLASS__, $params);
    }

}