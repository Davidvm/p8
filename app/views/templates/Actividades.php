<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/karma/app/assets/img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="CodePixar">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Karma Actividades</title>
  <!--
    CSS
    ============================================= -->
<?php include(ROOT . "/karma/app/views/templates/common/styles.php"); ?>

</head>

<body>

<?php include(ROOT . "/karma/app/views/templates/common/navigation_menu.php"); ?>

	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Actividades</h1>
					<nav class="d-flex align-items-center">
						<a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="#">Actividades</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-5">
				<div class="sidebar-categories">
					<div class="head">Categorias</div>
					<ul class="main-categories">
						<?php foreach ($categorias as $cat) { ?>
						<li class="main-nav-list">
							<span><input type="checkbox" class="common_selector category" value="<?php echo $cat['idCategoria']?>"></span> <?php echo $cat['nombre']?></span>
						</li>
						<?php }?>
					</ul>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8 col-md-7">
				<!-- Start Filter Bar -->
				<div class="filter-bar d-flex flex-wrap align-items-center">
					<h2 style="color: white; margin-top: 15px;">Actividades</h2>
				</div>
				<!-- End Filter Bar -->
				<!-- Start Best Seller -->
				<section class="lattest-product-area pb-40 category-list">
					<div class="row filter_data">
						<?php foreach ($actividades as $actividad) { ?>
						
						<!-- single product -->
						<div class="col-lg-4 col-md-6">
							<div class="single-product">
								<img class="img-fluid" src="app/assets/img/activities/<?php echo $actividad['Imagen_principal']; ?>" alt="">
								<div class="product-details">
									<h6><?php echo $actividad['nombre']; ?></h6>
									<div class="price">
									 	<?php if ($actividad['pvp_desc']){ ?>
                                        	<h6><?php echo $actividad['pvp_desc']; ?>€</h6>
                                        	<h6 class="l-through"><?php echo $actividad['pvp']; ?>€</h6>
                                     	<?php } else { ?>
	                                        <h6><?php echo $actividad['pvp']; ?>€</h6>
                                      	<?php } ?>
									</div>
									<div class="prd-bottom">

										<a href="<?php echo FOLDER_PATH . 'booking/?id='. $actividad['id']?>" class="social-info">
											<span class="ti-bag"></span>
											<p class="hover-text">Reservar</p>
										</a>
										<a href="<?php echo FOLDER_PATH . 'actividad/?id='. $actividad['id']?>" class="social-info">
											<span class="lnr lnr-move"></span>
											<p class="hover-text">Ver más</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
				</section>
				<!-- End Best Seller -->
				
			</div>
		</div>
	</div>

<?php include(ROOT . "/karma/app/views/templates/common/footer.php"); ?>
<?php include(ROOT . "/karma/app/views/templates/common/scripts.php"); ?>
<script type="text/javascript">

$(document).ready(function(){	


    function filter_data() {
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var action = 'fetch_data';
        var category = get_filter('category');
        $.ajax({
            url:"categoryprocess",
            method:"POST",
            data:{action:action, category:category},
            success:function(data){
                $('.filter_data').html(data);
            }
        });
    }

    function get_filter(class_name){
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        return filter;
    }


    $('.common_selector').click(function(){
        filter_data();
    });
});
</script>

</body>

</html>