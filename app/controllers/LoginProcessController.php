<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH .'app/models/LoginModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Login controller
*/
class LoginProcessController {
    private $model;
    private $session;

    public function __construct() {
        $this->model = new LoginModel();
        $this->session = new Session();
    }

    public function exec($request_params) {

        if ($this->verify($request_params)){
            header('Content-Type: application/json');
            echo json_encode( "El usuario y password son obligatorios.");
            exit();
        }

        $result = $this->model->signIn($request_params['username'], $request_params['password']);

        if ($result == 'failed'){
            header('Content-Type: application/json');
            echo json_encode( "Ha ocurrido algún error.");
            exit();
        } else {
            $this->session->init();
            $this->session->add('username', $result['username']);
            $this->session->add('email', $result['email']);
            $this->session->add('nombre', $result['nombre']);
            $this->session->add('id', $result['id']);
            header('Content-Type: application/json');
            echo json_encode($result);
            exit();
        }
    }

    private function verify($request_params) {
        return empty($request_params['username']) OR empty($request_params['password']);
    }

}