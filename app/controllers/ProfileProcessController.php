<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH .'app/models/UsuarioModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Profile controller
*/
class ProfileProcessController {

    private $model;
    private $session;

    public function __construct() {
        $this->model = new Usuario();
        $this->session = new Session();
    }

    public function exec($request_params) {

        $user = new Usuario();

        $user->idUsuario = $_REQUEST['id_user'];
        $user->username = $_REQUEST['username'];
        $user->nombre = $_REQUEST['nombre'];
        $user->apellidos = $_REQUEST['apellidos'];
        $user->email = $_REQUEST['email'];
        $user->telefono = $_REQUEST['telefono'];
        $user->contrasena = password_hash( $_REQUEST['password'], PASSWORD_BCRYPT, array('cost' => 11));
        $user->fechaNacimiento = date("Y-m-d", strtotime($_REQUEST['fechaNacimiento']));
        $user->joinDate = date('Y-m-d');

        $result = $this->model->updateUser($user);
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }
}