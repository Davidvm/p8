<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Login Model
 */
class LoginModel extends Database {

    protected $nombre;
    protected $contrasena;
    /**
     * Inicia conexión DB
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }


    public function signIn($user, $password) {

        try {
            $connection = Database::instance();
            $query = $connection->prepare("SELECT * FROM usuario WHERE username=:username ");
            $query->bindParam(':username',$user,PDO::PARAM_STR);
            $query->execute();

            $row =  $query->fetch(PDO::FETCH_ASSOC);

            if (!empty($row)) { 
                if (password_verify($password, $row['contrasena'])) {
                    return $row;
                } else {
                    return 'failed';
                }
            } else {
                return 'failed';
            }

        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
    }
}