<?php
defined('BASEPATH') or exit('No se permite acceso directo');

// Valores de uri
define('URI', $_SERVER['REQUEST_URI']);
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);

// Valores de rutas
define('FOLDER_PATH', '/karma/');
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PATH_VIEWS', FOLDER_PATH . '/app/views/');
define('PATH_CONTROLLERS', 'app/controllers/');
define('HELPER_PATH', 'system/helpers/');
define('ASSETS', ROOT . FOLDER_PATH . "app/assets/");
define('LIBS_ROUTE', ROOT . FOLDER_PATH . 'system/libs/');

// Valores de core
define('CORE', 'system/core/');
define('DEFAULT_CONTROLLER', 'Home');

//Valores de base de datos
define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', 'root');
define('DB_NAME', 'karma');


/**Valores de base de produccion
define('HOST', 'localhost');
define('USER', 'dvm2015_user');
define('PASSWORD', 'holakase2019');
define('DB_NAME', 'dvm2015_karma'); **/


// Valores configuracion
define('ERROR_REPORTING_LEVEL', -1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//Otros valores
define('ADMIN_EMAIL','david.valera.martinez@gmail.com');