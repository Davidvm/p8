<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH .'app/models/BookingModel.php';
require_once ROOT . FOLDER_PATH .'app/models/RegisterModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Booking Process controller
*/
class BookingProcessController {

    private $modelUser;
    private $modelBooking;
    private $session;

    public function __construct() {
        $this->modelBooking = new Booking();
        $this->modelUser = new RegisterModel();
        $this->session = new Session();
    }

    public function exec($request_params) {

        $user = new Usuario();
        $booking = new Booking();

        if ($_REQUEST['id_usuario'] == '0'){
            $user->username = $_REQUEST['user_name'];
            $user->nombre = $_REQUEST['nombre'];
            $user->apellidos = $_REQUEST['apellidos'];
            $user->email = $_REQUEST['email'];
            $user->telefono = $_REQUEST['telefono'];
            $user->contrasena = password_hash( $_REQUEST['password'], PASSWORD_BCRYPT, array('cost' => 11));
            $user->fechaNacimiento = date("Y-m-d", strtotime($_REQUEST['fechaNacimiento']));
            $user->joinDate = date('Y-m-d');
            $user_registered = $this->modelUser->insertUser($user);
        } else {
            $user_registered = $_REQUEST['id_usuario'];
        }

        $booking->id = $user_registered;
        $booking->idActividad = $_REQUEST['actividad'];
        $booking->idDia = $_REQUEST['dia'];
        $booking->idHora = $_REQUEST['hora'];
        $booking->pax = $_REQUEST['participantes'];
        $booking->pvp = $_REQUEST['pvp_total'];
        $booking->estado = 0;
        $booking->nota = $_REQUEST['nota'];

        $result = $this->modelBooking->insertBooking($booking);

        if ($result == 1){
            $this->sendEmail($_REQUEST['email']);
            header('Content-Type: application/json');
            echo json_encode('success');
            exit();
        } else {
            header('Content-Type: application/json');
            echo json_encode( "Ha ocurrido algún error.");
            exit();
        }
    }


    public function sendEmail($email){
        $to = $email;
        $from = ADMIN_EMAIL;

        $headers = "From: $from";
        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $subject = "Has realizado una reserva en Karma";

        $body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>{$subject}</title></head><body>";
        $body .= "<p> Hemos recibido tu reserva, en breve nos pondremos en contacto contigo. Saludos.";

        $body .= "</p>";
        $body .= "</body></html>";

        $send = mail($to, $subject, $body, $headers);
    }
}