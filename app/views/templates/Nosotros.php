<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/karma/app/assets/img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="CodePixar">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Nosotros</title>
  <!--
    CSS
    ============================================= -->
<?php include(ROOT . "/karma/app/views/templates/common/styles.php"); ?>

</head>

<body>

<?php include(ROOT . "/karma/app/views/templates/common/navigation_menu.php"); ?>
	
	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Nosotros</h1>
					<nav class="d-flex align-items-center">
						<a href="home">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="#">Nosotros</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<!-- Start Sample Area -->
	<section class="sample-text-area">
		<div class="container">
			<h3 class="text-heading">Sobre...</h3>
			<p class="sample-text">
				Somos una empresa formada por jóvenes emprendedores, amantes de la naturaleza y el deporte. Nuestro objetivo es acercar a las personas hacia nuevas disciplinas deportivas, dar a conocer una manera diferente de sentir la naturaleza y proporcionar una manera de escapar de la zona de confort.

			</p>
		</div>
	</section>
	<!-- End Sample Area -->
	
	<!-- Start Align Area -->

	<div class="whole-wrap pb-100" style="margin-bottom: 58px;">
		<div class="container">
			<div class="section-top-border">
				<h3 class="mb-30">Nuestra filosofía</h3>
				<div class="row">
					<div class="col-md-3">
						<img src="app/assets/img/elements/d.jpg" alt="" class="img-fluid">
					</div>
					<div class="col-md-9 mt-sm-20 mb-30">
						<p>En Karma Sports contamos con un equipo de deportistas profesionales, cada uno especializado en un sector, que te acompañaran en las actividades aportando su conocimiento y experiencia.
Nosotros nos hacemos cargo de todo, escogemos el alojamiento más cercano al lugar en el que se van a desarrollar las actividades y proveemos los materiales y equipamiento necesarios.
 Al finalizar la experiencia se realiza un aperitivo con todos los participantes donde se hace entrega de las fotos de vuestra aventura</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Align Area -->

<?php include(ROOT . "/karma/app/views/templates/common/footer.php"); ?>
<?php include(ROOT . "/karma/app/views/templates/common/scripts.php"); ?>

</body>

</html>