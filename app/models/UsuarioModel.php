<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
 * Usuario Model
 */
class Usuario extends Database
{

    public $id;
    public $username;
    public $nombre;
    public $apellidos;
    public $email;
    public $telefono;
    public $contrasena;
    public $fechaNacimiento;
    public $fechaRegistro;


    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }


    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * @param mixed $fechaNacimiento
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    /**
     * @return mixed
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * @param mixed $fechaRegistro
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->idRol;
    }

    /**
     * @param mixed $idRol
     */
    public function setIdRol($idRol)
    {
        $this->idRol = $idRol;
    }

    public function getProfileUser($id) {
        $connection = Database::instance();
        try {
            $stm = $connection->prepare("SELECT * FROM usuario WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function updateUser($data) {
        $connection = Database::instance();

        try {
            $sql = $connection->prepare("UPDATE usuario SET username = :uname, nombre = :nombre, apellidos = :apellidos,email = :email, telefono = :telefono, contrasena = :pass,fechaNacimiento = fnan,fechaRegistro = :jdate WHERE id = ?");

                $sql->bindParam(":uname",$data->username);
                $sql->bindParam(":nombre",$data->nombre);
                $sql->bindParam(":apellidos",$data->apellidos);
                $sql->bindParam(":email",$data->email);
                $sql->bindParam(":telefono",$data->telefono);
                $sql->bindParam(":pass",$data->contrasena);
                $sql->bindParam(":fnan",$data->fechaNacimiento);
                $sql->bindParam(":jdate",$data->joinDate);

                if($sql->execute()) {


                } else {

                    echo "Query could not execute !";
                }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}