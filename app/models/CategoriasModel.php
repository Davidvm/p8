<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Categorias Model
 */
class Categorias extends Database {

    public $idCategoria;
    public $categoria;

  /**
  * Inicia conexión DB
  */
  public function __construct()
  {
    parent::__construct();
  }

    /**
     * @return mixed
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @param mixed $idCategoria
     *
     * @return self
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     *
     * @return self
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }


    public function getAllCategories() {
        $connection = Database::instance();
        try {
            $stm = $connection->prepare("SELECT * FROM Categoria");
            $stm->execute();
            return $stm->fetchAll();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function getCategoriaById($id_categoria){
        $connection = Database::instance();
        try {
            $stm = $connection->prepare("SELECT * FROM categoria WHERE idCategoria = ?");
            $stm->execute(array($id_categoria));
            return $stm->fetch();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}