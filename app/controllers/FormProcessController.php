<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
* Form Process controller
*/
class FormProcessController {

    public function __construct() {

    }

    public function exec($request_params) {

        $name = strip_tags(htmlspecialchars($_REQUEST['name']));
        $email_address = strip_tags(htmlspecialchars($_REQUEST['email']));
        $phone = strip_tags(htmlspecialchars($_REQUEST['subject']));
        $message = strip_tags(htmlspecialchars($_REQUEST['message']));
            
        $to = ADMIN_EMAIL;
        $from = strip_tags(htmlspecialchars($_REQUEST['email']));
        $name = strip_tags(htmlspecialchars($_REQUEST['name']));
        $subject = strip_tags(htmlspecialchars($_REQUEST['subject']));
        $message = strip_tags(htmlspecialchars($_REQUEST['message']));

        $headers = "From: $from";
        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $subject = "Tienes una consulta de tu web";

        $body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Express Mail</title></head><body>";
        $body .= "<table style='width: 100%;'>";
        $body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
        $body .= "</td></tr></thead><tbody><tr>";
        $body .= "<td style='border:none;'><strong>Nombre:</strong> {$name}</td>";
        $body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
        $body .= "</tr>";
        $body .= "<tr><td style='border:none;'><strong>Asunto:</strong> {$subject}</td></tr>";
        $body .= "<tr><td></td></tr>";
        $body .= "<tr><td colspan='2' style='border:none;'>{$message}</td></tr>";
        $body .= "</tbody></table>";
        $body .= "</body></html>";

        $send = mail($to, $subject, $body, $headers);

        echo 'success';
    }
}