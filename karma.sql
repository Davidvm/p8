-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2020 a las 16:55:59
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `karma`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) COLLATE utf8_bin NOT NULL,
  `descripcion` text COLLATE utf8_bin NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `pvp` int(5) NOT NULL,
  `pvp_desc` int(11) DEFAULT NULL,
  `Imagen_principal` varchar(60) COLLATE utf8_bin NOT NULL,
  `Imagen_secundaria_1` varchar(60) COLLATE utf8_bin NOT NULL,
  `Imagen_secundaria_2` varchar(60) COLLATE utf8_bin NOT NULL,
  `Imagen_secundaria_3` varchar(60) COLLATE utf8_bin NOT NULL,
  `recomendaciones` varchar(60) COLLATE utf8_bin NOT NULL,
  `ropa_calzado` varchar(60) COLLATE utf8_bin NOT NULL,
  `comida` varchar(60) COLLATE utf8_bin NOT NULL,
  `material` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`id`, `nombre`, `descripcion`, `idCategoria`, `pvp`, `pvp_desc`, `Imagen_principal`, `Imagen_secundaria_1`, `Imagen_secundaria_2`, `Imagen_secundaria_3`, `recomendaciones`, `ropa_calzado`, `comida`, `material`) VALUES
(1, 'Sky de montaña', 'El esquí de montaña combina una parte aeróbica en el ascenso, por lo que es necesario tener unas mínimas condición físicas que nos permitan realizar la actividad con normalidad y eso significa ser capaz de subir a ritmo tranquilo pero constante lo que dure la subida que recorremos. Hay que ser capaz es hacer una media de 300-350 metros de desnivel a la hora por lo que una ascensión de 600-1000 metros de desnivel puede llevarnos entre 2 horas hasta 3 o 4 horas. ', 2, 350, 285, 'p1.jpg', 'p2.jpg', 'p3.jpg', 'p4.jpg', '', '', '', ''),
(2, 'Snowboard', 'El snowboard,2​ snowboarding, tabla sobre nieve, tabla de nieve, tabla neval o incluso tablanieve o nevotabla, es un deporte extremo de invierno, en el que se utiliza una tabla para deslizarse sobre una pendiente cubierta por nieve. El equipo básico para practicarlo son la mencionada tabla, las fijaciones y las botas.', 2, 350, NULL, 'p8.jpg', 'p9.jpg', 'p10.jpg', 'p11.jpg', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad_detalles`
--

CREATE TABLE `actividad_detalles` (
  `idDetalles` int(11) NOT NULL,
  `idActividad` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `diaId` int(11) NOT NULL,
  `horaId` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `actividad_detalles`
--

INSERT INTO `actividad_detalles` (`idDetalles`, `idActividad`, `pax`, `diaId`, `horaId`, `estado`) VALUES
(1, 1, 20, 1, 1, 1),
(2, 2, 15, 2, 1, 1),
(3, 1, 20, 3, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idCategoria` int(11) NOT NULL,
  `nombre` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idCategoria`, `nombre`) VALUES
(1, 'Agua'),
(2, 'Tierra'),
(3, 'Aire');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dia_actividad`
--

CREATE TABLE `dia_actividad` (
  `id` int(11) NOT NULL,
  `dia` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dia_actividad`
--

INSERT INTO `dia_actividad` (`id`, `dia`) VALUES
(1, '2020-05-23'),
(2, '2020-05-22'),
(3, '2020-06-05'),
(4, '2020-06-12'),
(5, '2020-06-19'),
(6, '2020-05-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horas_actividad`
--

CREATE TABLE `horas_actividad` (
  `id` int(11) NOT NULL,
  `inicioActividad` time NOT NULL,
  `finActividad` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `horas_actividad`
--

INSERT INTO `horas_actividad` (`id`, `inicioActividad`, `finActividad`) VALUES
(1, '09:00:00', '13:00:00'),
(2, '12:00:00', '15:00:00'),
(3, '10:00:00', '14:00:00'),
(4, '09:30:00', '15:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `idReserva` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idActividad` int(11) NOT NULL,
  `idDia` int(11) NOT NULL,
  `idHora` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `pvp` float NOT NULL,
  `estado` int(11) NOT NULL,
  `nota` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`idReserva`, `idUser`, `idActividad`, `idDia`, `idHora`, `pax`, `pvp`, `estado`, `nota`) VALUES
(1, 4, 1, 1, 1, 3, 1050, 0, 'saddasdasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(45) COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_bin NOT NULL,
  `email` varchar(60) COLLATE utf8_bin NOT NULL,
  `telefono` int(15) NOT NULL,
  `contrasena` varchar(150) COLLATE utf8_bin NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `fechaRegistro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `nombre`, `apellidos`, `email`, `telefono`, `contrasena`, `fechaNacimiento`, `fechaRegistro`) VALUES
(1, 'Test', 'Benito', 'Camelas', 'test@gmail.com', 971855858, '$2y$11$SuXsofr14ETxhvufF3aODuMvnZ0jERSkU3YW/xPD7J.pnHJCPl4BS', '2020-05-03', '2020-05-20'),
(2, 'Test2', 'Juan', 'Antonio', 'juan@gmail.com', 971525858, '$2y$11$KYvfeKoWqrCDVebnQeCyler9hmm8OwZ.//Hh98Ns6vh/vLUcu5fsm', '2020-05-01', '2020-05-20'),
(3, 'Test3', 'Fran', 'Soisa', 'testassad@gmail.com', 85674542, '$2y$11$ejbBrlx2ZdlaojgRa/GyGed6yIqh/k.d7Jf4qY0qteFqZI98//hj6', '2020-05-01', '2020-05-20'),
(4, 'Test4', 'Deivid', 'ASDasd', 'asdasd@gmail.com', 971545858, '$2y$11$qXambxZuSxTxhVnTzutoJeNvpASV44RwC.MK2ZVQYp2IptbYroIW.', '2020-05-01', '2020-05-20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categoria` (`idCategoria`) USING BTREE;

--
-- Indices de la tabla `actividad_detalles`
--
ALTER TABLE `actividad_detalles`
  ADD PRIMARY KEY (`idDetalles`),
  ADD KEY `fk_id_actividad` (`idActividad`),
  ADD KEY `fk_id_dia` (`diaId`),
  ADD KEY `fk_id_hora` (`horaId`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `dia_actividad`
--
ALTER TABLE `dia_actividad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `horas_actividad`
--
ALTER TABLE `horas_actividad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`idReserva`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `actividad_detalles`
--
ALTER TABLE `actividad_detalles`
  MODIFY `idDetalles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dia_actividad`
--
ALTER TABLE `dia_actividad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `horas_actividad`
--
ALTER TABLE `horas_actividad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `idReserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `fk_subcategoria` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `actividad_detalles`
--
ALTER TABLE `actividad_detalles`
  ADD CONSTRAINT `fk_id_actividad` FOREIGN KEY (`idActividad`) REFERENCES `actividad` (`id`),
  ADD CONSTRAINT `fk_id_dia` FOREIGN KEY (`diaId`) REFERENCES `dia_actividad` (`id`),
  ADD CONSTRAINT `fk_id_hora` FOREIGN KEY (`horaId`) REFERENCES `horas_actividad` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
