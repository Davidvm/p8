<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/karma/app/assets/img/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="CodePixar">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Registro</title>
  <!--
    CSS
    ============================================= -->
<?php include(ROOT . "/karma/app/views/templates/common/styles.php"); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<?php include(ROOT . "/karma/app/views/templates/common/navigation_menu.php"); ?>

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Registro</h1>
                    <nav class="d-flex align-items-center">
                        <a href="home">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Registro</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->
    <section class="checkout_area section_gap">
        <div class="container">
            <div class="check_title">
                <h2>Introduce los datos para registrarte</h2>
            </div>
           <div class="col-md-6"> <a href="" onclick="window.history.go(-1); return false;">Volver</a></div>
            <form class="row contact_form m-t-30" id="registerForm" method="post">
              <div id="msg" class="message"></div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Nombre de Usuario*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Repite la Contraseña*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" id="datepicker" class="form-control" name="fechaNacimiento" placeholder="Fecha de Nacimiento*">
                </div>
                <div class="col-md-4 form-group p_star">
                    <input type="text" id="telefno" class="form-control" name="telefono" placeholder="Teléfono*">
                </div>

                <div class="col-md-12 form-group">
                    <button type="submit" class="primary-btn" id="btn-submit">Registrarse</button>
                </div>
            </form>
        </div>
    </section>
    <!--================End Checkout Area =================-->

<?php include(ROOT . "/karma/app/views/templates/common/footer.php"); ?>
<?php include(ROOT . "/karma/app/views/templates/common/scripts.php"); ?>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>

<script type="text/javascript">
  $('document').ready(function(){

    $( "#datepicker" ).datepicker({
        dateFormat: "dd-mm-yy"
    });

    /* validation */
    $("#registerForm").validate({
        rules:
        {
            user_name: {
                required: true,
                minlength: 3
            },
            nombre: {
                required: true,
            },
            apellidos: {
                required: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            telefono: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            user_name: "Introduce un Usuario valido",
            password:{
                required: "Introduce una contraseña",
                minlength: "La contraseña debe tener al menos 8 caracteres"
            },
            email: "Introduce un email valido",
            telefono: "Introduce un teléfono",
            cpassword:{
                required: "Repite la contraseña",
                equalTo: "Las contraseñas no son iguales"
            }
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm() {
      var form = $('#registerForm'); // contact form

          $.ajax({
              url: 'registerprocess', // form action url
              type: 'POST', // form submit method get/post
              dataType: 'html', // request type html/json/xml
              data: form.serialize(), // serialize form data
              success: function(data) {
              if(data == 1){

                    $("#msg").fadeIn(1000, function(){

                        $("#msg").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> ¡El email ya esta en la base de datos!</div>');

                    });

                } else if(data.idUsuario) {

                     window.location.href = "login";

                } else{

                    $("#error").fadeIn(1000, function(){

                        $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span>'+ data +' !</div>');

                    });
                }
            }
        });
        return false;
    }
    /* form submit */

});
</script>


</body>

</html>