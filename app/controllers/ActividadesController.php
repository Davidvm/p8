<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/CategoriasModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/ActividadesModel.php';

/**
 * ActividadesController
 */
class ActividadesController extends Controller {

  public $categorias;
  public $actividades;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->categorias = new Categorias();
    $this->actividades = new Actividades();

  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();
  }

  /**
  * Método de ejemplo
  */
  public function show()
  {
    $params = array('categorias' => $this->categorias->getAllCategories(), 'actividades' => $this->actividades->getAllActividades());
    $this->render(__CLASS__, $params); 
  }

}