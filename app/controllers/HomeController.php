<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/ActividadesModel.php';
require_once ROOT . '/karma/app/models/HomeModel.php';
/**
 * Home controller
 */
class HomeController extends Controller {

  public $model;
  public $actividades;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->model = new HomeModel();
    $this->actividades = new Actividades();
  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();
  }

  /**
  * Método de ejemplo
  */
  public function show()
  {
    $params = array('actividades' => $this->actividades->getRamdonActividades(2));
    $this->render(__CLASS__, $params); 
  }


  public function usuario($param)
  {
    $res = $this->model->getUser($param);
    $this->nombre = $res['usuario_dev'];
    $this->show();
  }


}