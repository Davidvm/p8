<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/CategoriasModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/ActividadesModel.php';
require_once ROOT . FOLDER_PATH . '/app/models/BookingModel.php';

/**
 * Booking controller
 */
class BookingController extends Controller {

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->categorias = new Categorias();
    $this->actividad = new Actividades();
    $this->booking = new Booking();

  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();

  }

  public function show() {
      $id_actividad = $_GET['id'];
      $actividad = $this->actividad->getActividad($id_actividad);
      $categoria = $this->categorias->getCategoriaById($actividad['idCategoria']);
      $dias = $this->booking->getAlldaysOfActivity($id_actividad);
      $horas = $this->booking->getAllHoursOfActivity($id_actividad);
      $params = array('actividad' => $actividad, 'categoria' => $categoria, 'dias' => $dias, 'horas' => $horas);
      $this->render(__CLASS__, $params);
  }

}